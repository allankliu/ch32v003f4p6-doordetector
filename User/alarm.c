/*
 * alarm.c
 *
 *  Created on: Jan 26, 2024
 *      Author: Allan K Liu
 *
 *  The simple project is based upon CH32V003F4P6 (TSSOP) from WCH. The application
 *  is detect a GPIO attached to the door of a refrigerator door. Once the door is
 *  open, the GPIO falling edge will wake up the controller, to repeatedly checking
 *  the IO status. It will start to alarm if the timeout is reached. When the door is
 *  close, the controller will drop into sleep mode. Occasionally, it will wake up
 *  every hour to check the battery health. So we need the following features to be
 *  archived in the firmware.
 *
 *  The code is from EVT of QH. Some code is from the following post:
 *  https://www.cnblogs.com/Li-Share/p/16793638.html
 *
 *  - GPIO External interrupt for wake up from PSM mode
 *  - DBG mode, for develop and maintenance purposes.
 *  - Timer management
 *  - ADC for battery level checking
 *  - PWM for buzzer output
 *  - LED for simple UI
 *  - PSM mode, including GPIO and peripheral management
 *
 *  GPIO allocation:
 *  - EXIT: PD2
 *  - RXD: PD5
 *  - TXD: PD6
 *  - ADC:
 *  - RST:
 *  - PWM: PD2
 *  - LED: PD?
 *  - KEY1: PA2
 *  - KEY2: PA1
 *
 */

#include "alarm.h"

void alarm_Init(void){

}

void alarm_Wakeup_Handler(void){

}

void alarm_Enter_PSM(void){

}

void alarm_Setup_Params(void){

}

void alarm_VBat_Warning(void){

}
